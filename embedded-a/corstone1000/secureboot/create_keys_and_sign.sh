# Copyright (c) 2024 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

###############################################################################
# Corstone 1000 UEFI Secureboot keys creation and signing script              #
#                                                                             #
# This script is responsible to create the UEFI Secureboot keys, sign the     #
# kernel images for both Corstone-1000 FVP and FPGA with these keys, and copy #
# the public keys, signed and unsigned kernel images to the ESP images for    #
# both the FVP and FPGA.                                                      #
#                                                                             #
# This script assumes the following :                                         #
# 1. corstone1000-esp-image has already been generated for both Corstone-1000 #
# FVP and FPGA.                                                               #
# 2. The corstone-1000 images have been compiled for both FVP and FPGA        #
#                                                                             #
# Please refer to the Corstone-1000 user guide for more details:              #
# https://corstone1000.docs.arm.com/                                          #
###############################################################################

#! /bin/bash

Usage()
{
	echo "Script to create keys and sign images for UEFI Secureboot "
	echo
	echo "Syntax: sh create_keys_and_sign.sh [-d <device type> -v <certification validity> |-h ]"
	echo
	echo "Options:"
	echo "d		Device type. Valid options: "fvp" or "mps3" "
	echo "v		Certification validity in days (Optional). If this option is not supplied, then default value is 365 days."
	echo "m		Mount point to mount the ESP image (Optional). Default mount point : /mnt/secureboot_test"
	echo "h		Print this usage."
	echo
}

if [ "$#" -eq 0 ]; then
        echo "Error: Invalid number of parameters"
        Usage
	exit
fi

# Default value of certificate validity is 365 days
CERT_VALIDITY=365

MOUNT_DIR=/mnt/test_secureboot

OPTSTRING=":d:v:m:lh"

while getopts ${OPTSTRING} opt; do
  case ${opt} in
	  v)
		  CERT_VALIDITY=$OPTARG
		  ;;
	  m)
		  MOUNT_DIR=$OPTARG
		  ;;
	  d)
		  if [ $OPTARG = "fvp" ]  || [ $OPTARG = "mps3" ]; then
		    DEVICE_TYPE=$OPTARG
		  else
		    echo "Error: Invalid device type"
		    exit
		  fi
		  ;;
	  h)
		  Usage
		  exit;;
	  :)
		  echo "Option -${OPTARG} requires an argument."
		  ;;
	  ?)
 		echo "Invalid option: -${OPTARG}."
      		exit 1
      		;;
	esac
done

echo "Starting the script to create keys and sign images for UEFI Secureboot"
echo "DEVICE_TYPE : $DEVICE_TYPE"
echo "CERT_VALIDITY : $CERT_VALIDITY"
echo

# Validate the requirements
which sign-efi-sig-list 1> /dev/null    || { echo "Please install 'efitools' Minimum version: 1.8.1" && exit 1; }
which sbsign            1> /dev/null    || { echo "Please install 'efitools' Minimum version: 1.8.1" && exit 1; }
which openssl           1> /dev/null    || { echo "Please install 'openssl'" && exit 1; }

INSTALL_KEYS_DIR=corstone1000_secureboot_keys
INSTALL_IMAGE_DIR=corstone1000_secureboot_${DEVICE_TYPE}_images
TEMP_DIR=temp
KERNEL_IMAGE=Image.gz-initramfs-corstone1000-$DEVICE_TYPE.bin
ESP_IMAGE=corstone1000-esp-image-corstone1000-$DEVICE_TYPE.wic
KERNEL_IMAGE_NAME=Image_$DEVICE_TYPE

if [ ! -f "$KERNEL_IMAGE" ]; then
	echo "$KERNEL_IMAGE does not exist. "
       	exit
fi
if [ ! -f "$ESP_IMAGE" ]; then
	echo "$ESP_IMAGE does not exist. "
       	exit
fi

# Generate a certificate
generate_key_cert() {
    name=$1
    openssl req -x509 -sha256 -newkey rsa:2048 -subj "/CN=Test $name/" -keyout $name.key -out $name.crt -nodes -days $CERT_VALIDITY
}

mkdir -p ${INSTALL_KEYS_DIR} 1> /dev/null
mkdir -p ${INSTALL_IMAGE_DIR} 1> /dev/null
mkdir -p ${TEMP_DIR} 1> /dev/null
cd ${TEMP_DIR} 1> /dev/null

echo "Creating the certificates"
# Create certificates
generate_key_cert "PK"
generate_key_cert "KEK"
generate_key_cert "db"
generate_key_cert "dbx"

# Generate efi signature list from the certificates created above
cert-to-efi-sig-list -g 11111111-2222-3333-4444-123456789abc PK.crt PK.esl
cert-to-efi-sig-list -g 11111111-2222-3333-4444-123456789abc KEK.crt KEK.esl
cert-to-efi-sig-list -g 11111111-2222-3333-4444-123456789abc db.crt db.esl
cert-to-efi-sig-list -g 11111111-2222-3333-4444-123456789abc dbx.crt dbx.esl

# Create empty signature list file in order to delete the PK variable after the secureboot testing
touch NULL.esl

# Now, sign the signature lists with their corresponding private keys
sign-efi-sig-list -c PK.crt -k PK.key PK PK.esl PK.auth 1> /dev/null;	sleep 1
sign-efi-sig-list -c PK.crt -k PK.key KEK KEK.esl KEK.auth 1> /dev/null;	sleep 1
sign-efi-sig-list -c KEK.crt -k KEK.key db db.esl db.auth 1> /dev/null;	sleep 1
sign-efi-sig-list -c KEK.crt -k KEK.key dbx dbx.esl dbx.auth 1> /dev/null;	sleep 1

sign-efi-sig-list -c PK.crt -k PK.key PK NULL.esl PK_delete.auth 1> /dev/null; 	sleep 1


# copy the authorization headers to the INSTALL_DIR
cp *.auth ../${INSTALL_KEYS_DIR}

# copy and sign the Linux kernel image
cp ../$KERNEL_IMAGE  $KERNEL_IMAGE_NAME.gz
# gunzip is required to unzip the kernel image, as sbsign tool doesn't recognize the compressed format
gunzip $KERNEL_IMAGE_NAME.gz

echo
echo "Sign the Linux kernel image"
# Now, it's time to sign the kernel image
sbsign --key db.key --cert db.crt $KERNEL_IMAGE_NAME
mv $KERNEL_IMAGE_NAME $KERNEL_IMAGE_NAME.signed ../${INSTALL_IMAGE_DIR}

cd ..

echo
echo "Copy the public keys and both signed and unsigned Linux kernel images in the ESP image"
# copy the public keys and the signed and unsigned images in ESP image
mkdir $MOUNT_DIR
mount -o rw,offset=1048576 $ESP_IMAGE $MOUNT_DIR
cp -a ${INSTALL_KEYS_DIR} ${INSTALL_IMAGE_DIR} $MOUNT_DIR 2> /dev/null
sync
echo *Content of ESP_IMAGE:*
ls $MOUNT_DIR
sleep 5
umount $MOUNT_DIR
sleep 1

rm -rf $MOUNT_DIR
rm -rf ${INSTALL_KEYS_DIR} ${INSTALL_IMAGE_DIR} ${TEMP_DIR}
exit


