# Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

###############################################################################
# Corstone 1000 NOPT image script                                             #
#                                                                             #
# After GPT changes in the latest release for corstone1000,                   #
# the build system does not create a nopt image anymore. However,             #
# nopt image is still used to create a capsule. This script creates           #
# a nopt image with fixed offset from necessary firmware images.              #
# The user still needs to run the capsule generation script from edk2         #
# using the output of this script to create a valid capsule for corstone1000. #
#                                                                             #
# Please refer to the Corstone-1000 user guide for more details:              #
# https://corstone1000.docs.arm.com/                                          #
###############################################################################

#!/bin/bash

tfm_offset=102400
fip_offset=479232
kernel_offset=2576384

############################################################
# Help                                                     #
############################################################
Help()
{
   # Display Help
   echo "NOPT image generation for uefi capsule"
   echo
   echo "Syntax: sh capsule_gen.sh [-d DeviceType|-h]"
   echo
   echo "options:"
   echo "d    Select device type: Valid options: 'fvp' or 'mps3'"
   echo "h    Print this Help."
   echo
}

while getopts ":d:lh" option; do
   case $option in
      d) # select device
        if [ "$#" -ne 2 ]; then
            echo "Error: Invalid number of parameters"
            Help
        fi
        if [ $OPTARG = "fvp" ]  || [ $OPTARG = "mps3" ]; then
            dd conv=notrunc bs=1 if=bl2_signed.bin of=corstone1000_image.nopt
            dd conv=notrunc bs=1 if=tfm_s_signed.bin of=corstone1000_image.nopt seek=${tfm_offset}
            dd conv=notrunc bs=1 if=signed_fip-corstone1000.bin of=corstone1000_image.nopt seek=${fip_offset}
            dd conv=notrunc bs=1 if=Image.gz-initramfs-corstone1000-$OPTARG.bin of=corstone1000_image.nopt seek=${kernel_offset}
            if [ -f "corstone1000_image.nopt" ]; then
                echo "corstone1000_image.nopt is created. You can use this image to generate a uefi capsule now!!"
            else
                echo 'Error: Wrong directory!!\nPlease go to;
                \n<_workspace>/build/tmp/deploy/images/corstone1000-${device-type}
                \nbefore runing the script.'
            fi
        else
             echo "Error: Invalid device type"
         fi
        ;;
      h) # display Help
         Help
         exit;;
      ?) # Invalid option
         echo "Error: Invalid option"
         Help
         exit;;
   esac
done

if [ $OPTIND -eq 1 ]; then echo "No options were passed"; 
    shift $((OPTIND-1))
    Help
fi

