# Arm SystemReady IR Patch

---

__🚨Disclaimer:__

*_Arm reference solutions are Arm public example software projects that track and pull upstream components, incorporating their respective security fixes published over time._*
*_Users of these solutions are responsible for ensuring that the components they use contain all the required security fixes, if and when they deploy a product derived from Arm reference solutions._*

---


[Arm SystemReady IR (Infrastructure Ready)][arm-systemready-ir-webpage] is a certification program developed by Arm for ensuring interoperability and compliance of embdedded Arm based SoCs with mainline Linux/BSD.

This repository provides patches and scripts to apply to the [meta-arm][meta-arm-git-repository] Yocto Project layer for components of the software stack used for Arm pre-integrated IP subsystems and system IP. They provide functionalities not available in the meta-arm Yocto Project layer to the Corstone-1000 software stack.

Currently supported Arm SoCs:
* [Corstone-1000][corstone-1000-webpage]



[arm-systemready-ir-webpage]: https://www.arm.com/architecture/system-architectures/systemready-certification-program/ir
[corstone-1000-webpage]: https://developer.arm.com/Processors/Corstone-1000
[meta-arm-git-repository]: https://github.com/jonmason/meta-arm
